#Team 10
#Chwan-Hao Tung     ctung003
#Joseph Wu          jwu039
#CS165 Project 1,  Bruteforce /etc/shadow

import time
import multiprocessing
import base64
import string
import hashlib
from itertools import product
from itertools import chain

#bonus:$1$hfT7jp2q$yKkGOHLs7BZiNuh03um670:16653:0:99999:7:::
#team10:$1$hfT7jp2q$wh5MlWjVniktm6zI5jpf3.:16653:0:99999:7:::

salt = 'hfT7jp2q'

#function for crypting the salt and password to generate a shadow password hash
#based on the algorithm described on http://www.vidarholen.net/contents/blog/?p=32
def crypt(password, salt):
    #generate alternate sum with KEY SALT KEY
    alternate = hashlib.md5(password + salt + password).digest()
    #generate the intial intermediate sum by concatenating ...
    intermediate = hashlib.md5()
    #hash of password + magic string + salt
    intermediate.update(password + "$1$" + salt)
    #hash of the length of password number of bytes of the alternate sum
    for i in range(0, len(password)):
        intermediate.update(alternate[i])
    #appending nul byte or first byte depending if the bit of the length of the password is 1 or 0 
    lengthbit = str(bin(len(password))[2:])
    for i in reversed(lengthbit):
        if i == '1':
            intermediate.update('\x00')
        else:
            intermediate.update(password[0])
    nextsum = intermediate.digest()
 
    #strengthening part
    for i in range(1000):
        nexti = hashlib.md5()
        #if i is odd, concatenate password  
        if i % 2:
            nexti.update(password)
        #if i is even, concatenate current intermediate
        else:
            nexti.update(nextsum)
        #if i is not divisible by 3, concatenate salt    
        if i % 3:
            nexti.update(salt)
        #if i is not divisible by 7, concatenate password 
        if i % 7:
            nexti.update(password)
        #if i is odd, concatenate password      
        if i % 2:
            nexti.update(nextsum)
        #if i is even, concatenate current intermediate    
        else:
            nexti.update(password)
        nextsum = nexti.digest()

    #start creating the finalhash we return
    finalhash = "$1$" + salt + "$"
    
    #this is the special md5 crypt base64 character set
    nintendo64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    
    actualhashedpw = ''
    
    #rearraging the bits of the final intermediate sum according to the algorithm
    pickedbits = ord(nextsum[11]) << 120 | ord(nextsum[4]) << 112 | ord(nextsum[10]) << 104 | ord(nextsum[5]) << 96 | ord(nextsum[3]) << 88 | ord(nextsum[9]) << 80 | ord(nextsum[15]) << 72 | ord(nextsum[2]) << 64 | ord(nextsum[8]) << 56 | ord(nextsum[14]) << 48 | ord(nextsum[1]) << 40 | ord(nextsum[7]) << 32 | ord(nextsum[13]) << 24 | ord(nextsum[0]) << 16 | ord(nextsum[6]) << 8 | ord(nextsum[12])
    
    #pick the characters based on the picked bits' least significant 6 bits, do it 22 times to pick out the 22 characters of the final hash
    for i in range(22):
        actualhashedpw += nintendo64[pickedbits & 0b111111] 
        pickedbits >>= 6
        
    finalhash += actualhashedpw
    return finalhash

#function for dividing work amongst the 5 processes, each process works with a different set of starting characters
def dividework(i):
    if i == 0:
        for c in "abcde":
            checkpw(c,i)
    if i == 1:
        for c in "fghij":
            checkpw(c,i)
    if i == 2:
        for c in "klmno":
            checkpw(c,i)
    if i == 3:
        for c in "pqrst":
            checkpw(c,i)
    if i == 4:
        for c in "uvwxyz":
            checkpw(c,i)                                

#function to generate passwords to hash and check against the given hash
def checkpw(starting_alphabet,section):
    count = 0
    result =""
    #chain all the products of possible combinations of less than or equal to 6 lowercase alphabetical characters
    for i in chain(product(starting_alphabet, repeat=1)
                                ,product(starting_alphabet, string.lowercase)
                                ,product(starting_alphabet, string.lowercase, string.lowercase)
                                ,product(starting_alphabet, string.lowercase, string.lowercase, string.lowercase)
                                ,product(starting_alphabet, string.lowercase, string.lowercase, string.lowercase, string.lowercase)
                                ,product(starting_alphabet, string.lowercase, string.lowercase, string.lowercase, string.lowercase, string.lowercase)):
        #create the clear text password
        cleartext = ''.join(i)
        #hash it using the md5-crypt algorithm
        target = crypt(str.encode(cleartext),str.encode(salt))
        count += 1
        #check against our target hash given to us (team10)
        if target == "$1$hfT7jp2q$wh5MlWjVniktm6zI5jpf3.":
            result = "wow, we did it, somehow. Password is " + cleartext + ". Took " + str(count) + " guesses.\n"
            print result
            filename = ('result' + str(section))
            f = open(filename,'w+')
            f.write(result)
            return result
        print target    
    return result   

#need this if statement for running this script on windows machines, or else it blows it up
if __name__ == '__main__':
    #create 5 processes
    for i in range(5):
        print i
        proc = multiprocessing.Process(target=dividework, args=(i,))
        proc.start() 
        



